# ImaGC

![imagc-icon](img/imagc.png)

## Intodução

```txt
Editor de Imagens mais simples que já existiu
porém, igualmente cheio de funções..

O ImaGC permite ao usuario:
- adicionar logotipos as imagens;
- redefine automaticamente o tamanho da imagem se necessário;
- converter uma imagem selecionada para icone (.ico);
```

***OBS: os logotipos devem ter uma mascara (fundo) transparente!***

## Demonstração

![demo-01](img/01.png)
![demo-02](img/02.png)

## Instruções e Informações

![instrucao-programa](img/03.png)
![informacao-programa](img/04.png)

## Erros

`falha ao visualizar imagem`
![falha-visualizar-imagem](img/05.png)

`erro ao adicionar logotipo a imagem`
![erro-adicionar-logo](img/06.png)

`erro ao converter para ico`
![erro-converter-ico](img/07.png)

**Para melhor experiência e confirmação da capacidade do programa \
disponibilizei nas pastas `./ImaGC-ico` e `./ImaGC-logo` algumas imagens exemplares \
convertidas em (.ico) e com logotipo adicionado..**

***Faça Bom Proveito!***

---

&copy; 2019-2021 [Nurul Carvalho](mailto:nuruldecarvalho@gmail.com) \
&trade; [ArtesGC](https://artesgc.home.blog)
